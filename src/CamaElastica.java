/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Duoc
 */
public class CamaElastica extends ArriendoJuegos {
    private String color;
    private String tamanio; //pequeña-mediana-grande

    public CamaElastica(String color, String tamanio, String nombre, int precioArriendo, String descripcion) {
        super(nombre, precioArriendo, 'E', descripcion);
        this.color = color;
        this.tamanio = tamanio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    @Override
    public String toString() {
        return super.toString() + " CamaElastica{" + "color=" + color + ", tamanio=" + tamanio + '}';
    }

    @Override
    public void descuento() {
        int descuento = (int)(getPrecioArriendo()*DESCUENTO_EXTERIOR);
        setPrecioArriendo(getPrecioArriendo()-descuento);
    }
    
    
    
}
