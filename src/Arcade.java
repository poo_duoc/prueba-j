/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Duoc
 */
public class Arcade extends ArriendoJuegos {
    
    private String emulador;
    private int cantidadJuegos;

    public Arcade(String emulador, int cantidadJuegos, String nombre, int precioArriendo,String descripcion) {
        super(nombre, precioArriendo, 'I', descripcion);
        this.emulador = emulador;
        this.cantidadJuegos = cantidadJuegos;
    }

    public String getEmulador() {
        return emulador;
    }

    public void setEmulador(String emulador) {
        this.emulador = emulador;
    }

    public int getCantidadJuegos() {
        return cantidadJuegos;
    }

    public void setCantidadJuegos(int cantidadJuegos) {
        this.cantidadJuegos = cantidadJuegos;
    }
    
    @Override
    public void descuento() {
        int descuento = (int)(getPrecioArriendo()*DESCUENTO_INTERIOR);
        setPrecioArriendo(getPrecioArriendo()-descuento);
    }

    @Override
    public String toString() {
        return super.toString() + " Arcade{" + "emulador=" + emulador + ", cantidadJuegos=" + cantidadJuegos + '}';
    }
    
    
    
}
