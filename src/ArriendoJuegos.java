/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Duoc
 */
public abstract class ArriendoJuegos {
    
    public static double DESCUENTO_INTERIOR = 0.1;
    public static double DESCUENTO_EXTERIOR = 0.2;
    
    private String nombre;
    private int precioArriendo;
    private char categoria; //i = interior / e = exterior
    private String descripcion;

    public ArriendoJuegos(String nombre, int precioArriendo, char categoria, String descripcion) {
        this.nombre = nombre;
        this.precioArriendo = precioArriendo;
        this.categoria = categoria;
        this.descripcion = descripcion;
        descuento();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecioArriendo() {
        return precioArriendo;
    }

    public void setPrecioArriendo(int precioArriendo) {
        this.precioArriendo = precioArriendo;
    }

    public char getCategoria() {
        return categoria;
    }

    public void setCategoria(char categoria) {
        this.categoria = categoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "ArriendoJuegos{" + "nombre=" + nombre + ", precioArriendo=" + precioArriendo + ", categoria=" + categoria + ", descripcion=" + descripcion + '}';
    }
    
    public abstract void descuento();
    
}
